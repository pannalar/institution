package institution;

import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.StringJoiner;
import java.util.stream.Collectors;


public class InstitutionServiceArrayListImpl implements InstitutionService {
	List <Institution> institutionlist = new ArrayList <> ();
	//int count = 0 ;
	

	public void addInstitution(Institution institution) {
		if(institutionlist.size()>0 && institutionlist.contains(institution)) {
		System.out.println("Already Exist");
	}
		else {
			institutionlist.add(institution);
		}
		
	}


	@Override
	public void displayAll() {
		// TODO Auto-generated method stub
		institutionlist.forEach(System.out::print);
		/*for(institution data:institutionlist)
		System.out.println(data);
		}*/
		}
	


	@Override
	public void search(String name) {
		// TODO Auto-generated method stub
		{
			institutionlist.stream()
		      .filter(str -> str.getName().equals(name))
		      .collect(Collectors.toList());
			
			
			System.out.println(institutionlist);
		}
				/*for(institution search : institutionlist) {
					
					if(search.getName().equalsIgnoreCase(name)) {
					System.out.println(search);*/
			      
	}


	@Override
	public List<Institution> search(LocalDate establishedDate) {
		// TODO Auto-generated method stub
		return institutionlist.stream().filter(date1 -> date1.getEstablishedDate().isAfter(establishedDate)).collect(Collectors.toList());
		
	}
	



	@Override
	public void sort() {
		// TODO Auto-generated method stub
		
		Collections.sort (null) ;
		displayAll();
	
	}

	@Override
	public void delete(String name) {
		// TODO Auto-generated method stub
		for(Iterator<Institution> itr = institutionlist.iterator();itr.hasNext();) {
			Institution name1=itr.next();	
			if (name1.getName().equals(name))
			{
			itr.remove();
			System.out.println(" DELETED SUCESSFULLY");
			}
			
	}
	}

	@Override
	public void downloadCSVFile() throws IOException {
		// TODO Auto-generated method stub

		FileWriter file=new FileWriter("download.csv");
		for(Institution inst : institutionlist)
		{
			file.write(convertTocsv(inst));
		}
			file.close();
	

	}


	@Override
	public void downloadJSONFile() throws IOException {
		// TODO Auto-generated method stub

		StringJoiner join = new StringJoiner(",", "[", "]");
		String data;
		FileWriter fileWriter = new FileWriter("downloadJson.json");
		for (Institution institution : institutionlist) 
		{
		data = convertTojson(institution);
		join.add(data);
		}
		// System.out.println(join.toString());
		fileWriter.write(join.toString());



		fileWriter.close();
}

public int sizeOf()

{
	  
	        return institutionlist.size();  
}

	}
	