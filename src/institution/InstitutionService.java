package institution;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

public interface InstitutionService {
	void addInstitution(Institution institution);

	 void displayAll();
	 
	 void search(String name);

	List<Institution>search(LocalDate establishedDate);
	 void sort();
	 
	 void delete(String name);
	  
	 default String convertTocsv(Institution institution)
	 {
		 
	StringBuffer str= new StringBuffer();
   str.append (institution.getName());
   str.append(" ,");
   str.append(institution.getEstablishedDate());
   str.append(" ,");
   str.append(institution.getCity());
   str.append(" ,");
   str.append(institution.getEmailId());
   str.append(" ,");
   str.append(institution.getPhoneNumber());
   str.append(",");
   str.append(institution.getId());
   str.append(",");
	return str.toString();
	

	 }

	 default String convertTojson(Institution institution)
	 {  
		 


return new StringBuffer().append("{").
		append("\n").append("\"name\":").append("\"").append(institution.getName())
				.append("\"").append(",").append("\n").append("\"id\":").append("\"").append(institution.getId())
				.append("\"").append(",").append("\n").append("\"emailId\":").append("\"").append(institution.getEmailId())
				.append("\"").append(",").
				append("\n").append("\"phoneNumber\":").append("\"").append(institution.getPhoneNumber())
				.append("\"").append(",").
				append("\n").append("\"city\":").append("\"").append(institution.getCity())
				.append("\"").append(",").append("\n").append("\"establishedDate\":").append("\"").append(institution.getEstablishedDate())
				.append("\"").append(",").append("}").toString();


	 
	 }
	 void downloadCSVFile()throws IOException;

	 void downloadJSONFile()throws IOException;
	}
