package institution;

import java.util.Properties;

public abstract class InstitutionServiceFactory {
	public static InstitutionService  getInstitutionService(Properties properties)
	{
		String type=properties.getProperty("implementation");
		type=type.toUpperCase();
		InstitutionService service=null;
		switch(type)
		{
		case "ARRAY":
			service=new InstitutionServiceArrayImpl();
			break;
		case "ARRAYLIST":
			service=new InstitutionServiceArrayListImpl();
			break;
			
		case "DATABASE":
			
			String driver=properties.getProperty("driver");
			String url=properties.getProperty("url");
			String user=properties.getProperty("user");
			String password=properties.getProperty("password");
			service=new InstitutionServiceDatabaseImp(driver,url,user,password);
			break;
			
			
		}
		return service;
	}}
