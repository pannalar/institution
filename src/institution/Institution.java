package institution;


import java.time.LocalDate;
import java.util.Objects;

public class Institution {

	private int id;
	private String name;
	private LocalDate establishedDate;
	private String city;
	private String emailId;
	private int phoneNumber;
	
	public Institution(int id, String name, LocalDate establishedDate, String city, String emailId, int phoneNumber) {
		
		this.id= id;
		this.name=name;
		this.establishedDate= establishedDate;
		this.city= city;
		this.emailId=emailId;
		this.phoneNumber=phoneNumber;
	}


  public int getId() {
		return id;
	}





	public void setId(int id) {
		this.id = id;
	}





	public String getName() {
		return name;
	}





	public void setName(String name) {
		this.name = name;
	}





	public LocalDate getEstablishedDate() {
		return establishedDate;
	}





	public void setEstablishedDate(LocalDate establishedDate) {
		this.establishedDate = establishedDate;
	}





	public String getCity() {
		return city;
	}





	public void setCity(String city) {
		this.city = city;
	}





	public String getEmailId() {
		return emailId;
	}





	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}





	public int getPhoneNumber() {
		return phoneNumber;
	}





	public void setPhoneNumber(int phoneNumber) {
		this.phoneNumber = phoneNumber;
	}





	public Institution () {
	}
	

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Institution other = (Institution) obj;
		return Objects.equals(city, other.city) && Objects.equals(emailId, other.emailId)
				&& Objects.equals(establishedDate, other.establishedDate) && id == other.id
				&& Objects.equals(name, other.name) && Objects.equals(phoneNumber, other.phoneNumber);
	}

	public String toString() {
		StringBuffer str = new StringBuffer();
		str.append("Institution name = " + name);
		str.append("\nInstitution id = "+ id);
		str.append("\nEstablished date =" + establishedDate);
		str.append("\nPhone number =" + phoneNumber);
		str.append("\nEmail Id =" + emailId);
		return str.toString();
		}

	public int compareTo(Institution InstitutionService) {
		if(this.establishedDate.isAfter(InstitutionService.getEstablishedDate()))
		return -1;
		else if(this.establishedDate.isBefore(InstitutionService.getEstablishedDate()))
		return 1;
		else
		return 0;

	
	
	}
}