package institution;

import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringJoiner;

public class InstitutionServiceArrayImpl implements InstitutionService{
	
		 private Institution institution[];
				
		 private int j;
		
	    
		 	public  InstitutionServiceArrayImpl()
		 	{
		
		 		institution=new Institution[50];
		 	}
		 
		 	public  InstitutionServiceArrayImpl(int size)
		 	{	
		
		 		institution=new Institution[size];
		 	}

			@Override
			public void addInstitution(Institution institution1) {
				// TODO Auto-generated method stub
				boolean value=true;
	 			if(j>=institution.length)
		 
	 				System.out.println("ARRAY IS FULL");
		 
		
	 			if (value==true) 
	 				{	
			
		 
	 					for (int i=0;i<j;i++)
	 						{
	 							if(institution[i].equals(institution1))
	 								{
	 					System.out.println("ALREADY IN THE LIST");
	 				value=false;
			
	 							}
			 
	 						}
	 				}
	 			
		 if (value==true)
				 { 
			 institution[j]=institution1;
		 j++;
		 
				 }
		 }
			

			@Override
			public void displayAll() {
				// TODO Auto-generated method stub
				System.out.println();
 				if(institution[0]==null)
 					System.out.println("ENTRIES NOT FOUND");
	 
 				else
 					{
 					for(int i=0;i<j;i++)
 						{
 							System.out.println();
			 
			 System.out.println(institution[i]);
			
		 }
		 System.out.println();
	 }
			}

			@Override
			public void search(String name) {
				// TODO Auto-generated method stub
				boolean status=false;
				 
				 for(int i=0;i<j;i++)
				 {
					 if(institution[i].getName().equalsIgnoreCase(name))
					 {
						 status=true;
						 System.out.println(institution[i]);
					 }
				 }
				 
				 if(status==false)
				
				 
					 System.out.println(" INSTITUTION NOT FOUND ");
				 
			}

			@Override
			public List<Institution> search(LocalDate establishedDate) {
				// TODO Auto-generated method stub

				List<Institution>institutionlist = new ArrayList<Institution>();
				
				 for(int i=0;i<j;i++)
				 {
					 if(institution[i].getEstablishedDate().isAfter(establishedDate))
					 {
						 institutionlist.add(institution[i]);
						
					 }
				 } 
				
				return institutionlist;
				}
				

			

			@Override
			public void sort() {
				// TODO Auto-generated method stub

				 Arrays.sort(institution,0,j);
				 displayAll();
				 /*for(int i=0;i<j;i++)
				{
					 System.out.println(institution[i]); 
				}*/
			}

			@Override
			public void delete(String name) {
				// TODO Auto-generated method stub
				int k = 0;
				for (int i = 0; i < j; i++) {
				if (!institution[i].getName().equals(name)) {
					institution[k] = institution[i];
				k++;
				System.out.println("The person with +name+ is deleted");
				}

				}
				j=k;
			}

			@Override
			public void downloadCSVFile() throws IOException {
				// TODO Auto-generated method stub
				FileWriter file=new FileWriter("download.csv");
				for(int i=0;i<j;i++) {
					String result=convertTocsv(institution[i]);
					file.write(result);
				}
				file.close();
			}

			@Override
			public void downloadJSONFile() throws IOException {
				// TODO Auto-generated method stub
				StringJoiner join = new StringJoiner(",", "[", "]");
				String data;
				FileWriter fileWriter = new FileWriter("downloadJson.json");
				for (int i = 0; i < j; i++) 
					{
						data = convertTojson(institution[i]);
						join.add(data);
					}
				
				fileWriter.write(join.toString());
				fileWriter.close();
				}
				
			}	 


	