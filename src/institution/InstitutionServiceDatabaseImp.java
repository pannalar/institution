package institution;

import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

public class InstitutionServiceDatabaseImp implements InstitutionService {
	
	String username,password,url,driver;
    Connection connection;
    public InstitutionServiceDatabaseImp(String driver,String username,String password, String url) {

    	
    	this.driver= driver;
    	this.username=username;
    	this.password= password;
    	this.url=url;
    }
	
    private Connection getConnection() throws ClassNotFoundException, SQLException{
    	Class.forName(driver);
    	return DriverManager.getConnection(url,username,password);
    }
    
    private Institution getInstitution(ResultSet resultSet ) {
    Institution institution = new Institution();
   
    try {
    	
    	institution.setName(resultSet.getString("name"));
    	institution.setEstablishedDate(resultSet.getDate("establishedDate") .toLocalDate());
    	institution.setCity(resultSet.getString("city"));
    	institution.setEmailId(resultSet.getString("emailID"));
    	institution.setPhoneNumber(resultSet.getInt("phonenumber")) ;
    	}
    catch (SQLException e) {
    	e.printStackTrace();
    }
return institution;
	}
@Override
public void addInstitution(Institution institution) {
	try {
		Connection connection = getConnection();
		String query = "INSERT INTO institution(id,name,establisheddate,city,emaidID,phoneNumber) VALUES(?,?,?,?,?,?)";
		PreparedStatement preparedStatement = connection.prepareStatement(query);
		
	    preparedStatement.setString(1,institution.getName());
		preparedStatement.setDate(2,Date.valueOf(institution.getEstablishedDate()));
		preparedStatement.setString(3,institution.getCity());
		preparedStatement.setString(4,institution.getEmailId());
		preparedStatement.setInt(5,institution.getPhoneNumber());
		preparedStatement.executeUpdate();
		connection.close();
	}
	catch (Exception e) {
		e.printStackTrace();
		System.err.println(e.getClass().getName()+": "+e.getMessage());	
		System.exit(0);
	}
	System.out.println("opened database sucessfully");
		
	}

	@Override
	public void displayAll() {
		Connection connection ;
		try {
			connection = getConnection();
			String query = "SELECT * from institution";
			
				//String name = result.getname("name")
				
			//	System.out.println("Name: "+ name);
			//	System.out.println("ID: "+id);
				
				
			connection.close();
		}
		catch(ClassNotFoundException | SQLException  e) {
			e.printStackTrace();
		}
	}
			
		// TODO Auto-generated method stub


	@Override
	public void search(String name) {
		// TODO Auto-generated method stub
try {
	Connection connection;
	connection = getConnection();
	PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM institution where name=?");
	preparedStatement.setString(1, name);
	ResultSet resultSet = preparedStatement.executeQuery();
	while(resultSet.next())
	{
Institution instituion = getInstitution(resultSet);
System.out.println(instituion);

//String name = resultSet.getString("name")
//int id = result.getInt("int")
	}
	
	connection.close();
}
catch(ClassNotFoundException | SQLException e) {
	e.printStackTrace();
}
	}
	
	
	@Override
	
		public void sort() {
			try {
				Connection connection;
				connection = getConnection();
				PreparedStatement preparedstatement = connection.prepareStatement("SELECT * FROM institution ORDER by name");
				ResultSet resultSet = preparedstatement.executeQuery();
				while (resultSet.next())
				{
					Institution institution = getInstitution(resultSet);
					System.out.println(institution);
//					String name = resultSet .getString("name")
					
					//System.out.println("name:"+ name)	;
				}
				connection. close();
			}
				catch (ClassNotFoundException | SQLException e) {
					e.printStackTrace();
					System.err.println(e.getClass().getName() + " : " + e.getMessage());
					System.exit(0);
				}
				System.out.println("Sort displayed");


				}

	@Override
	public void delete(String name) {
		try {
			connection = getConnection();
			PreparedStatement preparedstatement = connection.prepareStatement("SELECT * FROM institution where name=?");
			preparedstatement.setString(1, name);
		preparedstatement.executeUpdate();
				System.out.println("Sucessfully Deleted");
			
		connection.close();
	}
		catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}}

	

	public List<Institution> search(LocalDate establishedDate) {
		List<Institution> institutionDateSearch = new ArrayList<Institution>();
		try {
			Connection  connection;
			connection = getConnection();
			PreparedStatement preparedstatement = connection.prepareStatement("SELECT * FROM institution where establishedDate=?");
			preparedstatement.setDate(1, Date.valueOf(establishedDate));
			ResultSet resultSet = preparedstatement.executeQuery();
			while (resultSet.next())
			{
				Institution institution = getInstitution(resultSet);
				System.out.println(institution);
			}
			
		//	String name = resultSet .getString("name")
			
			//System.out.println("name:"+ name)	;	
			// TODO Auto-generated method stub
		connection.close();
	}
		catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		return (institutionDateSearch);
	}
	
		



@Override
public void downloadCSVFile() throws IOException {
	// TODO Auto-generated method stub
	Connection connection;
	try {
	connection = getConnection();
	PreparedStatement pstmt = connection.prepareStatement("select * from institution");
	ResultSet resultSet = pstmt.executeQuery();
	FileWriter file = new FileWriter("download.csv");
	while (resultSet.next()) {

	String data =convertTocsv(getInstitution(resultSet)).toString();
	file.write(data);
	}
	file.close();
	} catch (ClassNotFoundException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
	} catch (SQLException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
	}
	
}



@Override
public void downloadJSONFile() throws IOException {
	// TODO Auto-generated method stub
	
	Connection connection;
		
		
		
		try {
		connection = getConnection();
		PreparedStatement pstmt = connection.prepareStatement("select * from institution");
		ResultSet resultSet = pstmt.executeQuery();
		FileWriter file = new FileWriter("download1.json");
		StringJoiner join = new StringJoiner(",", "[", "]");
		while (resultSet.next()) {
		String data = join.add(convertTojson(getInstitution(resultSet))).toString();
		file.write(data);
		
		
		
		}
		file.close();
		connection.close();
		} catch (ClassNotFoundException | SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		}



}
	
	
}

